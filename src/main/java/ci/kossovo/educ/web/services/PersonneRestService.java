package ci.kossovo.educ.web.services;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import ci.kossovo.educ.entity.Personne;
import ci.kossovo.educ.exceptions.InvalidEducException;
import ci.kossovo.educ.metier.IPersonneMetier;
import ci.kossovo.educ.web.models.Reponse;
import ci.kossovo.educ.web.utilitaires.Static;

@CrossOrigin(origins = { "http://localhost:4200", "http://localhost:4300" })
@RestController
public class PersonneRestService {

	@Autowired
	IPersonneMetier metierPersonne;

	@Autowired
	private ObjectMapper jsonMapper;

	@Value("${dos.images}")
	private String rep_images;

	// Methode local
	private Reponse<Personne> getPersonne(Long id) {
		// On recupère la personne
		Personne pers = null;
		try {
			pers = metierPersonne.find(id);
		} catch (RuntimeException e) {
			return new Reponse<>(1, Static.getErreurforexception(e), null);
		}

		// personne existe?
		if (pers == null) {
			List<String> messages = new ArrayList<>();
			messages.add(String.format("La personne [%s] n'existe pas", id));
			return new Reponse<Personne>(2, messages, null);
		}
		// OK
		return new Reponse<Personne>(0, null, pers);
	}

	///// Methode local
	private Reponse<Personne> getPersonnCni(String cni) {
		// On recupère la personne
		Personne pers = null;
		try {
			pers = metierPersonne.findByCni(cni);
		} catch (RuntimeException e) {
			return new Reponse<>(1, Static.getErreurforexception(e), null);
		}

		// personne existe?
		if (pers == null) {
			List<String> messages = new ArrayList<>();
			messages.add(String.format("La personne [%s] n'existe pas", cni));
			return new Reponse<Personne>(2, messages, null);
		}
		// OK
		return new Reponse<Personne>(0, null, pers);
	}

	////////////////////////////////////////////////////////////////////////////////////////////

	// Créé une personne
	@PostMapping("/personnes")
	public String creer(@RequestBody Personne p) throws JsonProcessingException {
		Reponse<Personne> reponse;

		try {
			Personne personne = metierPersonne.creer(p);

			List<String> messages = new ArrayList<>();
			messages.add(
					String.format("[%s] [%s] a été creer avec succès!", personne.getId(), personne.getNomComplet()));
			reponse = new Reponse<Personne>(0, messages, personne);
		} catch (InvalidEducException e) {
			reponse = new Reponse<Personne>(1, Static.getErreurforexception(e), null);
		}
		return jsonMapper.writeValueAsString(reponse);
	}

	// Modifier une personne
	@PutMapping("/personnes")
	public String modifier(@RequestBody Personne p) throws JsonProcessingException {
		Reponse<Personne> reponse;
		Reponse<Personne> reponseModif = getPersonne(p.getId());
		if (reponseModif.getStatus() == 0) {
			try {
				Personne personne = metierPersonne.modifier(p);
				List<String> messages = new ArrayList<>();
				messages.add(String.format("[%s] [%s] a été modifier avec succès!", personne.getId(),
						personne.getNomComplet()));

				reponse = new Reponse<Personne>(0, messages, personne);
			} catch (InvalidEducException e) {
				reponse = new Reponse<Personne>(1, Static.getErreurforexception(e), null);
			}
		} else {
			reponse = new Reponse<Personne>(reponseModif.getStatus(), reponseModif.getMessages(),
					reponseModif.getBody());
		}

		return jsonMapper.writeValueAsString(reponse);
	}

	// Trouver une personne par son id
	@GetMapping("/personnes/{id}")
	public String findById(@PathVariable("id") Long id) throws JsonProcessingException {
		Reponse<Personne> reponse;
		reponse = getPersonne(id);
		return jsonMapper.writeValueAsString(reponse);
	}

	// La liste des types de personnes.
	@GetMapping("/typepersonnes/{type}")
	public String findAll(@PathVariable("type") String type) throws JsonProcessingException {
		Reponse<List<Personne>> reponse = null;
		// liste des personnes
		try {
			List<Personne> personnesTous = metierPersonne.personneAll(type);

			if (!personnesTous.isEmpty()) {
				reponse = new Reponse<List<Personne>>(0, new ArrayList<>(), personnesTous);
			} else {
				List<String> messages = new ArrayList<>();
				messages.add("Pas de personnes enregistrées à ce jour");
				reponse = new Reponse<List<Personne>>(3, messages, new ArrayList<>());
			}
		} catch (RuntimeException e) {
			reponse = new Reponse<List<Personne>>(1, Static.getErreurforexception(e), new ArrayList<>());
		}
		return jsonMapper.writeValueAsString(reponse);
	}

	public void spprimer(List<Personne> entities) {
		metierPersonne.spprimer(entities);
	}

	@DeleteMapping("/personnes/{id}")
	public String supprimer(@PathVariable("id") Long id) throws JsonProcessingException {
		// La reponse
		Reponse<Boolean> reponse = null;
		boolean erreur = false;
		Personne p = null;

		// On recupère la personne
		if (!erreur) {
			Reponse<Personne> reponsePers = getPersonne(id);
			p = reponsePers.getBody();
			if (reponsePers.getStatus() != 0) {
				reponse = new Reponse<>(reponsePers.getStatus(), reponsePers.getMessages(), false);
				erreur = true;
			}
		}
		if (!erreur) {
			// Suppression
			try {
				List<String> messages = new ArrayList<>();
				messages.add(String.format("[%s] [%s] a été bien supprimer!", p.getId(), p.getNomComplet()));
				reponse = new Reponse<>(0, messages, metierPersonne.supprimer(id));
			} catch (RuntimeException e) {
				reponse = new Reponse<>(3, Static.getErreurforexception(e), false);
			}

		}
		// La reponse
		return jsonMapper.writeValueAsString(reponse);
	}

	//// chercher une personne par mot clé.

	@GetMapping("/persrech/{type}")
	public String chercherPersonneParMc(@PathVariable("type") String typePers, @RequestParam String mc)
			throws JsonProcessingException {

		Reponse<List<Personne>> reponse = null;

		try {
			List<Personne> pers = metierPersonne.chercherPersonneParMc(typePers, mc);
			if (!pers.isEmpty()) {
				reponse = new Reponse<List<Personne>>(0, null, pers);
			} else {
				List<String> messages = new ArrayList<>();
				messages.add("Pas de personnes pour  ce mot clé");
				reponse = new Reponse<List<Personne>>(3, messages, new ArrayList<>());
			}
		} catch (Exception e) {
			reponse = new Reponse<List<Personne>>(1, Static.getErreurforexception(e), new ArrayList<>());
		}
		return jsonMapper.writeValueAsString(reponse);

	}

	public boolean existe(Long id) {
		return metierPersonne.existe(id);
	}

	public Long compter() {
		return metierPersonne.compter();
	}

	public Personne chercherParMatricule(String matricule) {
		return metierPersonne.chercherParMatricule(matricule);
	}

	public Personne chercherParIdentifiantS(String numCni) {
		return metierPersonne.chercherParIdentifiantS(numCni);
	}

	public List<Personne> chercherUserParMc(String mc) {
		return metierPersonne.chercherUserParMc(mc);
	}

	////// Photos ////////////////////

	// Ajout photo
	@PostMapping("/photos")
	public String ajoutPhoto(@RequestParam(name = "imageFile") MultipartFile file) throws JsonProcessingException {
		System.out.println("/////////////////////debut//////////////////////////////");
		System.out.println(rep_images);
		System.out.println("//////////////////////fin///////////////////////////////");
		String cni = file.getOriginalFilename();
		System.out.println("/////////////////////debut//////////////////////////////");
		System.out.println(cni);
		System.out.println("//////////////////////fin///////////////////////////////");
		Reponse<Personne> reponse = null;
		Reponse<Personne> reponseCni = getPersonnCni(cni);
		String chemin;

		if (reponseCni.getStatus() == 0) {
			Personne p = reponseCni.getBody();
			chemin = "http://localhost:8080/photos/" +p.getVersion()+"/"+ cni;
			System.out.println("/////////////////////debut//////////////////////////////");
			System.out.println(file);
			System.out.println("//////////////////////fin///////////////////////////////");

			if (!file.isEmpty()) {
				String dossier = rep_images + p.getType() + "/";
				System.out.println("/////////////////////Dossier//////////////////////////////");
				System.out.println(dossier);
				System.out.println("//////////////////////fin///////////////////////////////");
				File rep = new File(dossier);
				if (!rep.exists() || !rep.isDirectory()) {
					rep.mkdirs();
				}
				
				System.out.println("/////////////////////debut//////////////////////////////");
				System.out.println(rep);
				System.out.println("//////////////////////fin///////////////////////////////");
				try {
					// mètre à jpur personne
					p.setNomphoto(cni);
					p.setPathphoto(chemin);

					List<String> messages = new ArrayList<>();
					messages.add(String.format("[%s] [%s] a reçu sa photo avec succès!", cni, p.getNomComplet()));
					file.transferTo(new File(dossier + cni));
					reponse = new Reponse<Personne>(0, messages, metierPersonne.modifier(p));

				} catch (IllegalStateException | IOException | InvalidEducException e) {
					reponse = new Reponse<Personne>(1, Static.getErreurforexception(e), null);
				}

			} else {
				List<String> messages = new ArrayList<>();
				messages.add("Pas d'image a enregistrées");
				reponse = new Reponse<Personne>(6, messages, null);
			}

		} else {
			reponse = new Reponse<>(reponseCni.getStatus(), reponseCni.getMessages(), reponseCni.getBody());
		}

		return jsonMapper.writeValueAsString(reponse);
	}

	// Trouver une image
	// @RequestMapping(value = "/photos/{type}/{id}", produces =
	// MediaType.IMAGE_JPEG_VALUE)

	// Première version
	@GetMapping(value = "/photos/{vers}/{cni}", produces = MediaType.IMAGE_JPEG_VALUE)
	public byte[] getPhoto(@PathVariable String vers, @PathVariable String cni) throws FileNotFoundException, IOException {
		Reponse<Personne> reponseCni = getPersonnCni(cni);

		Personne p = reponseCni.getBody();
		String ver= vers;
		System.out.println("/////////////////////debut vers path//////////////////////////////");
		System.out.println(vers);
		System.out.println("/////////////////////debut getPhoto//////////////////////////////");
		System.out.println(p);
		System.out.println("//////////////////////fin get///////////////////////////////");
		File f = new File(rep_images + p.getType() + "/" + cni);
		System.out.println("/////////////////////debut getPhoto//////////////////////////////");
		System.out.println(f);
		System.out.println("//////////////////////fin get///////////////////////////////");
		byte[] img = IOUtils.toByteArray(new FileInputStream(f));

		return img;

	}

	// Deuxième version

	@GetMapping(value = "/photo/{cni}", produces = MediaType.IMAGE_JPEG_VALUE)
	public String getPhotos(@PathVariable String cni) throws FileNotFoundException, JsonProcessingException {
		Reponse<byte[]> reponse = null;
		Reponse<Personne> reponseCni = getPersonnCni(cni);
		Personne p = reponseCni.getBody();
		File f = new File(rep_images + p.getType() + "/" + cni);
		byte[] img;
		try {
			img = IOUtils.toByteArray(new FileInputStream(f));
			List<String> messages = new ArrayList<>();
			messages.add(String.format("[%s] a bien une photo !", cni));

			reponse = new Reponse<byte[]>(0, messages, img);
		} catch (IOException e) {
			reponse = new Reponse<byte[]>(7, Static.getErreurforexception(e), null);
		}

		return jsonMapper.writeValueAsString(reponse);

	}

	// Creer une personne avec photo

	// Créé une personne
	@PostMapping("/photos2")
	public String creerPersphoto(@RequestBody Personne p, @RequestParam(name = "image") MultipartFile file)
			throws JsonProcessingException {
		Reponse<Personne> reponse;

		try {
			List<String> messages = new ArrayList<>();
			if (!file.isEmpty()) {
				String dossier = rep_images + "/";
				File rep = new File(dossier);
				if (!rep.exists() || !rep.isDirectory()) {
					rep.mkdir();
				}

				try {
					// mètre à jpur personne
					p.setPathphoto(rep.getPath());

					file.transferTo(new File(dossier + file.getName()));
					messages.add(
							String.format("[%s] [%s] a reçu sa photo avec succès!", p.getType(), p.getNomComplet()));

				} catch (IllegalStateException | IOException e) {
					messages = Static.getErreurforexception(e);
				}
			}
			Personne personne = metierPersonne.creer(p);

			messages.add(
					String.format("[%s] [%s] a été creer avec succès!", personne.getId(), personne.getNomComplet()));
			reponse = new Reponse<Personne>(0, messages, personne);
		} catch (InvalidEducException e) {
			reponse = new Reponse<Personne>(1, Static.getErreurforexception(e), null);
		}
		return jsonMapper.writeValueAsString(reponse);
	}

}
