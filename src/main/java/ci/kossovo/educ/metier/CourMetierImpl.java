package ci.kossovo.educ.metier;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ci.kossovo.educ.dao.CourRepository;
import ci.kossovo.educ.dao.EtudiantCourRepository;
import ci.kossovo.educ.entity.Cours;
import ci.kossovo.educ.entity.EtudiantCours;

@Service
public class CourMetierImpl implements ICourMetier {
	
	@Autowired
	private CourRepository courRepository;
	@Autowired
	private EtudiantCourRepository etudiantCourRepository;

	@Override
	public Cours creer(Cours entity) {
		return courRepository.save(entity);
	}

	@Override
	public Cours modifier(Cours entity) {
		return courRepository.save(entity);
	}

	@Override
	public Cours find(Long id) {
		return courRepository.findById(id).get();
	}

	@Override
	public List<Cours> findAll() {
		return courRepository.findAll();
	}

	@Override
	public void spprimer(List<Cours> entities) {
		courRepository.deleteAll(entities);

	}

	@Override
	public boolean supprimer(Long id) {
		courRepository.deleteById(id);
		return true;
	}

	@Override
	public boolean existe(Long id) {
		return courRepository.existsById(id);
	}

	@Override
	public Long compter() {
		return courRepository.count();
	}

	@Override
	public EtudiantCours absence(Long idCour, Long idEtudiant, boolean absence, String motif) {
		return etudiantCourRepository.save(new EtudiantCours(idEtudiant, idCour, absence, motif));
	}

}
