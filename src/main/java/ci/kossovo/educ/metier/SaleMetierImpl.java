package ci.kossovo.educ.metier;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ci.kossovo.educ.dao.SaleRepository;
import ci.kossovo.educ.entity.Sale;

@Service
public class SaleMetierImpl implements ISaleMetier {
	
	@Autowired
	private SaleRepository saleRepository;

	@Override
	public Sale creer(Sale entity) {
		return saleRepository.save(entity);
	}

	@Override
	public Sale modifier(Sale entity) {
		return saleRepository.save(entity);
	}

	@Override
	public Sale find(Long id) {
		return saleRepository.findById(id).get();
	}

	@Override
	public List<Sale> findAll() {
		return saleRepository.findAll();
	}

	@Override
	public void spprimer(List<Sale> entities) {
		saleRepository.deleteAll(entities);

	}

	@Override
	public boolean supprimer(Long id) {
		saleRepository.deleteById(id);;
		return true;
	}

	@Override
	public boolean existe(Long id) {
		return saleRepository.existsById(id);
	}

	@Override
	public Long compter() {
		return saleRepository.count();
	}

}
