package ci.kossovo.educ.metier;

import ci.kossovo.educ.entity.Cours;
import ci.kossovo.educ.entity.EtudiantCours;

public interface ICourMetier extends IMetier<Cours, Long> {
	
	public EtudiantCours absence(Long idCour , Long idEtudiant, boolean absence, String motif);

}
