package ci.kossovo.educ.metier;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ci.kossovo.educ.dao.MatiereRepository;
import ci.kossovo.educ.entity.Matiere;
import ci.kossovo.educ.exceptions.InvalidEducException;

@Service
public class MatiereMetierImpl implements IMatiereMetier {
	@Autowired
	private MatiereRepository matiereRepository;

	@Override
	@Transactional
	public Matiere creer(Matiere entity) throws InvalidEducException {
		if (entity.getLibelle() == null || entity.getLibelle() == " ") {
			throw new InvalidEducException("Le libelle ne peut-être null!");
		}

		List<Matiere> mats = matiereRepository.findByLibelle(entity.getLibelle());
		if (!mats.isEmpty()) {
			throw new InvalidEducException("Ce libelle existe dejà.");
		}

		return matiereRepository.save(entity);
	}

	@Override
	@Transactional
	public Matiere modifier(Matiere entity) throws InvalidEducException {
		if (entity.getLibelle() == null || entity.getLibelle() == " ") {
			throw new InvalidEducException("Le libelle ne peut-être null!");
		}
		/*
		 * List<Matiere> mats= matiereRepository.findByLibelle(entity.getLibelle()); if
		 * (!mats.isEmpty()) { throw new
		 * InvalidEducException("Ce libelle existe dejà."); }
		 */

		return matiereRepository.save(entity);
	}

	@Override
	public Matiere find(Long id) {
		return matiereRepository.findById(id).get();
	}

	@Override
	public List<Matiere> findAll() {
		return matiereRepository.findAll();
	}

	@Override
	public void spprimer(List<Matiere> entities) {
		matiereRepository.deleteAll(entities);

	}

	@Override
	public boolean supprimer(Long id) {
		matiereRepository.deleteById(id);
		return true;
	}

	@Override
	public boolean existe(Long id) {
		return matiereRepository.existsById(id);
	}

	@Override
	public Long compter() {
		return matiereRepository.count();
	}

	@Override
	public List<Matiere> findByLibelle(String lib) {

		return matiereRepository.findByLibelle(lib);
	}

	@Override
	public List<Matiere> rechParMotCle(String mc) {

		return matiereRepository.rechParMotCle(mc);
	}

}
