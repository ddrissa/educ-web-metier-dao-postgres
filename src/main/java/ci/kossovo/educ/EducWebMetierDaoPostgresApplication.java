package ci.kossovo.educ;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EducWebMetierDaoPostgresApplication {

	public static void main(String[] args) {
		SpringApplication.run(EducWebMetierDaoPostgresApplication.class, args);
	}
}
