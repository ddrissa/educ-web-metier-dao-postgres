package ci.kossovo.educ.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import ci.kossovo.educ.entity.Evaluation;

public interface EvaluationRpository extends JpaRepository<Evaluation, Long> {

}
