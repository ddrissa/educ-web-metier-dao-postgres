package ci.kossovo.educ.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import ci.kossovo.educ.entity.EtudiantEvalu;
import ci.kossovo.educ.entity.EtudiantEvaluID;

public interface EtudiantEvaluRepository extends JpaRepository<EtudiantEvalu, EtudiantEvaluID> {

}
