package ci.kossovo.educ.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import ci.kossovo.educ.entity.Matiere;

public interface MatiereRepository extends JpaRepository<Matiere, Long> {
	List<Matiere> findByLibelle(String libelle);
	
	// Trouver matiere par mot clé
	@Query("select m from Matiere m where m.libelle like %:mc%")
	List<Matiere> rechParMotCle(@Param("mc") String mc);

}
