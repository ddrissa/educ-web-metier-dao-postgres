package ci.kossovo.educ.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import ci.kossovo.educ.entity.UserRole;

public interface UserRoleRepository extends JpaRepository<UserRole, Long> {

}
